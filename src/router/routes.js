
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    name: 'Default',
    redirect: { name: 'Fleets' },
    children: [
      {
        path: 'fleets',
        name: 'Fleets',
        component: () => import('pages/fleets/Index.vue'),
        children: [
          { path: ':groupId', name: 'GroupItems', component: () => import('pages/fleets/Group') },
          { path: ':groupId/:itemId', name: 'Item', component: () => import('pages/fleets/Item') }
        ]
      },
      { path: 'analytics', name: 'Analytics', component: () => import('pages/analytics/Index.vue') },
      { path: 'claims', name: 'Claims', component: () => import('pages/claims/Index.vue') },
      { path: 'policy', name: 'Policy', component: () => import('pages/policy/Index.vue') },
      { path: 'ntf', name: 'Ntf', component: () => import('pages/ntf/Index.vue') },
      { path: 'settings', name: 'Settings', component: () => import('pages/settings/Index.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
