import ScoreCirc from 'src/components/ScoreCirc'
import Widget from 'src/components/widget'

export default ({ Vue }) => {
  Vue.component(ScoreCirc.name, ScoreCirc)
  Vue.component(Widget.name, Widget)
}
