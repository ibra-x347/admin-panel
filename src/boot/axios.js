import axios from 'axios'

// const basePath = 'http://www.draivn.com/app-data/'
// const basePath = 'http://127.0.0.1:9090/'
const basePath = 'https://app-data.draivn.com/'

// { result: dict, errors: list }

export default ({ Vue, store }) => {
  Vue.prototype.$axios = axios

  const apiPlugin = {
    _cache: {}
  }

  // execute HTTP request, p - Promise
  apiPlugin.execRequest = async function execRequest (p) {
    // result
    let result = []
    // errors by model fields
    const errors = []

    let response = null
    try {
      response = await p
    } catch (e) {
      response = e.response
    }

    if (response) {
      result = (response.data && response.data.result) || []
      if (response.data && response.data.errors && response.data.errors.length) {
        response.data.errors.forEach(item => {
          errors.push(item.reason || 'Unknown error')
        })
      }
      if (response.status && response.status !== 200 && !errors.length) {
        errors.push(response.statusText || `Server request error, status=${response.status}`)
      }
    } else {
      errors.push('Unknown error')
    }

    return { result, errors }
  }

  apiPlugin.getUnits = async function () {
    const request = axios.get(`${basePath}fleetsMenu.json`)

    const res = await apiPlugin.execRequest(request)

    // const cKey = 'getUnits'
    // let res = apiPlugin._cache[cKey]
    //
    // if (!res) {
    //   const request = axios
    //     .get(`${basePath}devices/all`, {
    //       // headers: { Authorization: '' },
    //       params: {}
    //     })
    //   res = await apiPlugin.execRequest(request)
    //   if (res && res.result) {
    //     apiPlugin._cache[cKey] = res
    //   }
    // }

    return res
  }

  apiPlugin.getGroupWidgets = async function (groupId) {
    const cKey = `group:${groupId}`
    let res = apiPlugin._cache[cKey]

    if (!res) {
      const request = axios.get(`${basePath}group/${groupId}.json`)
      // const res = resp && resp.data
      // const request = axios
      //   .get(`${basePath}widgets/group/${groupId}`, {
      //     // headers: { Authorization: '' },
      //     params: {}
      //   })
      res = await apiPlugin.execRequest(request)
      if (res && res.result) {
        apiPlugin._cache[cKey] = res
      }
    }
    return res
  }

  apiPlugin.getDeviceWidgets = async function (groupId, deviceId) {
    const gw = await apiPlugin.getGroupWidgets(groupId)
    return {
      result: (gw && gw.result && gw.result.res_by_devices && gw.result.res_by_devices[parseInt(deviceId)]) || {},
      errors: (gw && gw.errors) || []
    }
  }

  Vue.prototype.$api = apiPlugin
  Vue.api = apiPlugin
}
